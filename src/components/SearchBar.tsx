import { StyleSheet, View } from 'react-native'

interface SearchBarProps {
  onEndEditing?: Function
  didTouch?: Function
  autofocus?: boolean
  onTextChange: Function
}

const SearchBar: React.FC<SearchBarProps> = ({
  onEndEditing,
  didTouch,
  autofocus = false,
  onTextChange
}) => {
  return (
    <View style={styles.container}>
      <View style={styles.searchBar}></View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'green',
    height: 60,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20
  },
  searchBar: {
    display: 'flex',
    height: 32,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#ededed',
    alignItems: 'center',
    borderRadius: 20,
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: '#E5E5E5',
    borderWidth: 2
  }
})

export { SearchBar }
