import { Address } from 'expo-location'
import { Dispatch } from 'react'
import { AsyncStorage } from 'react-native'

export interface UpdateLocationAction {
  readonly type: 'ON_UPDATE_LOCATION'
  payload: Address
}

export interface UserErrorAction {
  readonly type: 'ON_USER_ERROR'
  payload: any
}

export type UserAction = UpdateLocationAction | UserErrorAction

export const onUpdateLocation = (location: Address) => {
  return async (dispatch: Dispatch<UserAction>) => {
    try {
      await AsyncStorage.setItem('user_location', JSON.stringify(location))
      dispatch({
        type: 'ON_UPDATE_LOCATION',
        payload: location
      })
    } catch (error) {
      dispatch({
        type: 'ON_USER_ERROR',
        payload: error
      })
    }
  }
}
