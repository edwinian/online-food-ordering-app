import { Address } from 'expo-location'

export interface Category {
  title: string
  icon: string
}

export interface FoodModel {
  _id: string
  name: string
  description: string
  category: string
  price: string
  readyTime: number
  image: [string]
}

export interface Restaurant {
  _id: string
  name: string
  foodType: string
  address: string
  phone: string
  image: string
  foods: [FoodModel]
}

export interface FoodAvailability {
  categories: [Category]
  restaurants: [Restaurant]
  foods: [FoodModel]
}

export interface UserModel {
  firstName: string
  lastName: string
  contactNumber: string
  token: string
}

export interface UserState {
  user: UserModel
  location: Address
  error: string | undefined
}

export interface ShoppingState {
  availability: FoodAvailability
}
