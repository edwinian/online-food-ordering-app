import React, { useEffect } from 'react'
import { View, Text, StyleSheet, Dimensions, Image } from 'react-native'
import { connect } from 'react-redux'
import { ApplicationState } from '../redux/reducers'
import { onAvailability, ShoppingState, UserState } from '../redux'
import { SearchBar } from '../components'
import { useNavigation } from '../utils'

interface HomeScreenProps {
  userReducer: UserState
  shoppingReducer: ShoppingState
  onAvailability: Function
}

export const _HomeScreen: React.FC<HomeScreenProps> = ({
  userReducer,
  shoppingReducer,
  onAvailability
}: HomeScreenProps) => {
  const { location } = userReducer
  const { availability } = shoppingReducer
  const { categories, foods, restaurants } = availability
  const { navigate } = useNavigation()

  console.log('Foods, ', foods)

  useEffect(() => {
    onAvailability(location.postalCode)
  }, [])

  return (
    <View style={styles.container}>
      <View style={styles.navigation}>
        <View
          style={{
            marginTop: 50,
            flex: 4,
            backgroundColor: 'white',
            paddingHorizontal: 20,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'row'
          }}
        >
          <Text>{`${location.name}, ${location.street}, ${location.city}`}</Text>
          <Text>Edit Button</Text>
        </View>
        <View style={{ flex: 8, backgroundColor: 'green' }}>
          <SearchBar
            didTouch={() => {
              navigate('SearchPage')
            }}
            onTextChange={() => {}}
          />
        </View>
      </View>
      <View style={styles.body}>
        <Text> Home Screen </Text>
      </View>
      <View style={styles.footer}>
        <Text> Footer </Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'green'
  },
  navigation: {
    flex: 2,
    backgroundColor: 'red'
  },
  body: {
    flex: 9,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'yellow'
  },
  footer: {
    flex: 1,
    backgroundColor: 'cyan'
  }
})

const mapStateToProps = (state: ApplicationState) => ({
  userReducer: state.userReducer,
  shoppingReducer: state.shoppingReducer
})

const HomeScreen = connect(mapStateToProps, { onAvailability })(_HomeScreen)

export { HomeScreen }
